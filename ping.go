package ping

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"time"
	"github.com/labstack/echo"
)

func Ping(o chan time.Duration) error {
	host := "ya.ru"
	port := "80"
	timeout := time.Duration(1 * time.Second)
	t := time.Now()
	_, err := net.DialTimeout("tcp", host+":"+port, timeout)
	d := time.Now().Sub(t)
	if err != nil {
		// fmt.Printf("%s %s %s\n", host, "not responding", err.Error())
		return err
	} else {
		// fmt.Printf("%s %s %s %s\n", host, "responding on port:", port, d)
		o <- d
		return nil
	}
}

func get_mean_ping(n int) (time.Duration, error) {
	result := time.Duration(0)

	times := make(chan (time.Duration), n)
	go func() {
		defer close(times)
		for i := 0; i < n; i++ {
			go Ping(times)
		}
		time.Sleep(1 * time.Second)
	}()

	for t := range times {
		result += t
		// fmt.Println(result)
	}

	return result / (time.Duration(n)), nil
}

func old_main() {
	t := time.Now()

	go func() {
		for {
			mean_ping, err := get_mean_ping(15)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			fmt.Printf("mean ping for %s in %d attempts is %s\n", "ya.ru", 100, mean_ping)
			time.Sleep(time.Second * 1)
		}
	}()
	d := time.Now().Sub(t)
	fmt.Println(d)
	time.Sleep(time.Second * 10000)
}