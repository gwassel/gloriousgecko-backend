run:
	go run main.go

db:
	docker-compose up -d

stop_db:
	docker-compose down