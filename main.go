package main

import (
	"database/sql"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/labstack/echo"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
	"time"
)

type Host struct {
	ID              int    `json:"id" form:"id" query:"id"`
	Connection_data string `json:"connection_data" form:"connection_data" query:"connection_data"`
	User_data       string `json:"user_data" form:"user_data" query:"user_data"`
}

type Activity struct {
	ID        int       `json:"id" form:"id" query:"id"`
	Host_id   int       `json:"host_id" form:"host_id" query:"host_id"`
	Status    string    `json:"status" form:"status" query:"status"`
	Ping      int       `json:"ping" form:"ping" query:"ping"`
	Timestamp time.Time `json:"timestamp" form:"timestamp" query:"timestamp"`
}

// e.GET("/hosts/:user", hosts)
func hosts_by_user(c echo.Context) error {
	log.Println("hosts")
	user := c.Param("user")
	log.Println(user)
	rows, err := db.Query("SELECT * FROM host WHERE user_data = $1", user)
	if err != nil {
		log.Println(fmt.Errorf("hosts_by_user: %v", err))
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("hosts_by_user: %v", err))
	}
	defer rows.Close()

	hosts := make([]Host, 0)
	for rows.Next() {
		var host Host
		if err := rows.Scan(&host.ID, &host.Connection_data, &host.User_data); err != nil {
			log.Println(fmt.Errorf("hosts_by_user: %v", err))
			return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("hosts_by_user: %v", err))
		}
		hosts = append(hosts, host)
	}
	if err := rows.Err(); err != nil {
		log.Println(fmt.Errorf("hosts_by_user: %v", err))
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("hosts_by_user: %v", err))
	}
	log.Println(hosts)
	return c.JSON(http.StatusOK, hosts)
}

func saveHost(c echo.Context) error {
	h := new(Host)
	if err := c.Bind(h); err != nil {
		log.Printf(fmt.Sprintf("saveHost: %v", err))
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("addHost: %v", err))
	}
	if h.Connection_data == "" {
		return echo.NewHTTPError(http.StatusInternalServerError, "no connection_data provided")
	}
	if h.User_data == "" {
		return echo.NewHTTPError(http.StatusInternalServerError, "no user_data provided")
	}
	err := db.QueryRow("INSERT INTO host (user_data, connection_data) VALUES ($1, $2) returning id;", h.User_data, h.Connection_data).Scan(&h.ID)
	if err != nil {
		log.Printf(fmt.Sprintf("saveHost: %v", err))
		return echo.NewHTTPError(http.StatusInternalServerError, fmt.Errorf("addHost: %v", err))
	}
	log.Printf("created %v", h)
	return c.JSON(http.StatusCreated, h)
}

var db *sql.DB

func main() {
	var err error
	// init
	err = godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}
	psqlconn := os.Getenv("DATABASE")
	log.Println(psqlconn)

	// db
	// open database

	db, err = sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}
	// close database
	defer db.Close()

	// healthcheck database
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	log.Println("db is healthy")

	// web server
	e := echo.New()
	e.GET("/healthcheck", func(c echo.Context) error {
		return c.JSON(http.StatusOK, "OK")
	})
	e.GET("/hosts/:user", hosts_by_user)
	e.POST("/host", saveHost)
	e.Logger.Fatal(e.Start(":1323"))
}
